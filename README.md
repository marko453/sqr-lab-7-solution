# Lab 7 - Measure Failure Intensity

Student name: <b>Marko Pezer</b><br>
Student email: <b>m.pezer@innopolis.university</b><br>
Student group: <b>BS18-SE-01</b>

## Command

```
ab -n 12500 -c 175 -m "GET" "https://script.google.com/macros/s/AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w/exec?service=getSpec&email=m.pezer@innopolis.university"
```

## Screenshot

![image info](./screenshot.png)

## Calculation of Failure Intensity (FI)

```
MTTF = test_time / failure_count = 54940 / (10963 + 431) = 4.82183605
FI = 1 / MTTF = 1 / 4.82183605 = 0.20738988 ~ 0.2
```
